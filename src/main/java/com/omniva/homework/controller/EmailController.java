package com.omniva.homework.controller;

import com.omniva.homework.dto.EmailDto;
import com.omniva.homework.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class EmailController {

    private final EmailService emailService;

    @PostMapping("/email")
    public ResponseEntity<EmailDto> createEmail(@RequestBody EmailDto emailDto) {
        return ResponseEntity.ok(emailService.createEmail(emailDto));
    }

}
