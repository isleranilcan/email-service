package com.omniva.homework.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "email")
@AllArgsConstructor
@NoArgsConstructor
public class Email extends BaseEntity {

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "subject")
    private String subject;

    @Column(name = "message")
    private String message;

    @Column(name = "is_sent")
    private boolean isSent;

}
