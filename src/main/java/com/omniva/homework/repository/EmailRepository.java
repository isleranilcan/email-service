package com.omniva.homework.repository;

import com.omniva.homework.entity.Email;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmailRepository extends CrudRepository<Email, Long> {

    @Query("SELECT em from Email em WHERE em.isSent='FALSE'")
    List<Email> findUnsentEmails();

}
