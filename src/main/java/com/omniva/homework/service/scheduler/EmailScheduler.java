package com.omniva.homework.service.scheduler;

import com.omniva.homework.entity.Email;
import com.omniva.homework.service.EmailService;
import com.omniva.homework.service.sender.EmailSender;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

@Component
@RequiredArgsConstructor
public class EmailScheduler {

    private final EmailService emailService;
    private final EmailSender emailSender;

    Logger logger = Logger.getLogger(EmailScheduler.class.getName());

    @Scheduled(initialDelay = 3600000,
            fixedDelay = 3600000)
    public void run() {
        List<Email> unsentEmails = emailService.getUnsentEmails();
        unsentEmails.forEach(emailSender::sendEmail);

        logger.info("Scheduler just ran! " +
                "Number of emails sent: " + unsentEmails.size());
    }
}
