package com.omniva.homework.service;

import com.google.gson.Gson;
import com.omniva.homework.dto.EmailDto;
import com.omniva.homework.entity.Email;
import com.omniva.homework.mapper.EmailDtoMapper;
import com.omniva.homework.repository.EmailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
@RequiredArgsConstructor
public class EmailService {

    private final EmailRepository emailRepository;

    Logger logger = Logger.getLogger(EmailService.class.getName());

    public EmailDto createEmail(EmailDto emailDto) {
        Email email = new Email();
        email.setEmailAddress(emailDto.getEmailAddress());
        email.setSubject(emailDto.getSubject());
        email.setMessage(emailDto.getMessage());
        email.setSent(false);

        logger.info("Email is sent to repository: " + new Gson().toJson(email));

        return EmailDtoMapper.map(emailRepository.save(email));
    }

    public List<Email> getUnsentEmails() {
        return emailRepository.findUnsentEmails();
    }

    public void updateEmail(Email email) {
        emailRepository.save(email);
    }

}
