package com.omniva.homework.service.sender;


import com.omniva.homework.entity.Email;
import com.omniva.homework.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.logging.Logger;

@Component
@RequiredArgsConstructor
public class EmailSender {

    private final EmailService emailService;

    @Value("${mail.smtp.username}")
    private String username;

    @Value("${mail.smtp.apppassword}")
    private String password;

    Logger logger = Logger.getLogger(EmailSender.class.getName());

    public void sendEmail(Email email) {
        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(email.getEmailAddress())
            );
            message.setSubject(email.getSubject());
            message.setText(email.getMessage());

            Transport.send(message);

            logger.info("Email is sent to " + email.getEmailAddress());

            email.setSent(true);
            emailService.updateEmail(email);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }


}
