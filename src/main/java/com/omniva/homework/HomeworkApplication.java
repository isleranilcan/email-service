package com.omniva.homework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.logging.Logger;

@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
public class HomeworkApplication {

    static Logger logger = Logger.getLogger(HomeworkApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(HomeworkApplication.class, args);
        logger.info("h2 database started on: http://localhost:8080/h2-console");
    }

}
