package com.omniva.homework.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmailDto {
    private String emailAddress;
    private String subject;
    private String message;
}
