package com.omniva.homework.mapper;

import com.omniva.homework.dto.EmailDto;
import com.omniva.homework.entity.Email;

import java.util.List;
import java.util.stream.Collectors;

public class EmailDtoMapper {

    public static EmailDto map(Email email) {
        return EmailDto.builder()
                .emailAddress(email.getEmailAddress())
                .subject(email.getSubject())
                .message(email.getMessage())
                .build();
    }

    public static List<EmailDto> map(List<Email> emailList) {
        return emailList.stream().map(
                email -> EmailDto.builder()
                        .emailAddress(email.getEmailAddress())
                        .subject(email.getSubject())
                        .message(email.getMessage())
                        .build())
                .collect(Collectors.toList());

    }
}
