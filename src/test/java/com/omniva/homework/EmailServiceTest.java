package com.omniva.homework;

import com.omniva.homework.dto.EmailDto;
import com.omniva.homework.repository.EmailRepository;
import com.omniva.homework.service.EmailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class EmailServiceTest {

    @Mock
    private EmailRepository emailRepository;

    @Autowired
    private EmailService emailService;

    @Test
    @DisplayName("Should create email")
    public void testCreateEmail() {
        //given
        EmailDto expected = EmailDto.builder()
                .emailAddress("emailAddress")
                .subject("subject")
                .message("message")
                .build();
        when(emailRepository.save(any())).thenReturn(expected);

        //when
        EmailDto actual = emailService.createEmail(expected);

        //then
        assertEquals(expected, actual);
    }
}
