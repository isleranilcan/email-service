package com.omniva.homework;

import com.google.gson.Gson;
import com.omniva.homework.dto.EmailDto;
import com.omniva.homework.service.EmailService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class EmailControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private EmailService emailService;

    @Test
    public void testCreateEmail() throws Exception {
        //given
        EmailDto emailDto = EmailDto.builder()
                .emailAddress("emailAddress")
                .subject("subject")
                .message("message")
                .build();
        when(emailService.createEmail(emailDto)).thenReturn(mock(EmailDto.class));
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/v1/email")
                .accept(MediaType.APPLICATION_JSON).content(new Gson().toJson(emailDto))
                .contentType(MediaType.APPLICATION_JSON);

        //when
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        //then
        assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
    }
}
