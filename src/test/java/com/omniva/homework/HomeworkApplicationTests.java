package com.omniva.homework;

import com.omniva.homework.controller.EmailController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class HomeworkApplicationTests {

    @Autowired
    private EmailController emailController;

    @Test
    void contextLoads() {
        assertNotNull(emailController);
    }

}
