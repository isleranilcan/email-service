package com.omniva.homework;

import com.omniva.homework.dto.EmailDto;
import com.omniva.homework.entity.Email;
import com.omniva.homework.mapper.EmailDtoMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmailDtoMapperTest {

    @Test
    @DisplayName("Should map email to dto")
    public void testMap() {
        //given
        EmailDto expected = EmailDto.builder()
                .emailAddress("emailAddress")
                .subject("subject")
                .message("message")
                .build();
        Email email = new Email("emailAddress", "subject", "message", false);

        //when
        EmailDto actual = EmailDtoMapper.map(email);

        //then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Should map list of emails to dto list")
    public void testMapList() {
        //given
        EmailDto emailDto1 = EmailDto.builder()
                .emailAddress("emailAddress1")
                .subject("subject1")
                .message("message1")
                .build();
        EmailDto emailDto2 = EmailDto.builder()
                .emailAddress("emailAddress2")
                .subject("subject2")
                .message("message2")
                .build();
        List<EmailDto> expected = Arrays.asList(emailDto1, emailDto2);
        Email email1 = new Email("emailAddress1", "subject1", "message1", false);
        Email email2 = new Email("emailAddress2", "subject2", "message2", false);
        List<Email> emailList = Arrays.asList(email1, email2);

        //when
        List<EmailDto> actual = EmailDtoMapper.map(emailList);

        //then
        assertEquals(expected.size(), actual.size());
        assertEquals(expected.get(0), actual.get(0));
        assertEquals(expected.get(1), actual.get(1));
    }
}
